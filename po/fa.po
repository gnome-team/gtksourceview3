# Persian translation of gtksourceview.
# Copyright (C) 2004, 2005 Sharif FarsiWeb, Inc.
# This file is distributed under the same license as the gtksourceview package.
# Roozbeh Pournader <roozbeh@farsiweb.info>, 2004, 2005.
# Alireza Kheirkhahan <kheirkhahan@gmail.com> 2005.
# Meelad Zakaria <meelad@farsiweb.info>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: gtksourceview HEAD\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=gtksourceview\n"
"POT-Creation-Date: 2019-05-05 16:08-0700\n"
"PO-Revision-Date: 2005-08-20 23:13+0430\n"
"Last-Translator: Meelad Zakaria <meelad@farsiweb.info>\n"
"Language-Team: Persian <farsi@lists.sharif.edu>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:23
#, fuzzy
msgid "Disabled"
msgstr "متغیر"

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:24
msgid "Before"
msgstr ""

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:25
msgid "After"
msgstr ""

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:26
msgid "Always"
msgstr ""

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:32
msgid "Space"
msgstr ""

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:33
#, fuzzy
msgid "Tab"
msgstr "برچسب"

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:34
msgid "Newline"
msgstr ""

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:35
msgid "Non Breaking Whitespace"
msgstr ""

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:36
msgid "Leading"
msgstr ""

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:37
msgid "Text"
msgstr ""

#. (itstool) path: displayable-values/value@name
#: gtksourceview.xml:38
msgid "Trailing"
msgstr ""

#. (itstool) path: displayable-values/value@name
#. Translators: "All" is used as a label in the status bar of the
#. popup, telling that all completion pages are shown.
#: gtksourceview.xml:39 gtksourceview/gtksourcecompletion.c:833
msgid "All"
msgstr ""

#. (itstool) path: abnf.lang/language@_section
#. (itstool) path: actionscript.lang/language@_section
#. (itstool) path: ada.lang/language@_section
#. (itstool) path: ansforth94.lang/language@_section
#. (itstool) path: asp.lang/language@_section
#. (itstool) path: automake.lang/language@_section
#. (itstool) path: bennugd.lang/language@_section
#. (itstool) path: bluespec.lang/language@_section
#. (itstool) path: boo.lang/language@_section
#. (itstool) path: cg.lang/language@_section
#. (itstool) path: chdr.lang/language@_section
#. (itstool) path: c.lang/language@_section
#. (itstool) path: cobol.lang/language@_section
#. (itstool) path: cpphdr.lang/language@_section
#. (itstool) path: cpp.lang/language@_section
#. (itstool) path: csharp.lang/language@_section
#. (itstool) path: cuda.lang/language@_section
#. (itstool) path: d.lang/language@_section
#. (itstool) path: eiffel.lang/language@_section
#. (itstool) path: erlang.lang/language@_section
#. (itstool) path: forth.lang/language@_section
#. (itstool) path: fortran.lang/language@_section
#. (itstool) path: fsharp.lang/language@_section
#. (itstool) path: genie.lang/language@_section
#. (itstool) path: glsl.lang/language@_section
#. (itstool) path: go.lang/language@_section
#. (itstool) path: groovy.lang/language@_section
#. (itstool) path: haskell.lang/language@_section
#. (itstool) path: haskell-literate.lang/language@_section
#. (itstool) path: haxe.lang/language@_section
#. (itstool) path: idl.lang/language@_section
#. (itstool) path: java.lang/language@_section
#. (itstool) path: j.lang/language@_section
#. (itstool) path: kotlin.lang/language@_section
#. (itstool) path: lex.lang/language@_section
#. (itstool) path: llvm.lang/language@_section
#. (itstool) path: logtalk.lang/language@_section
#. (itstool) path: makefile.lang/language@_section
#. (itstool) path: meson.lang/language@_section
#. (itstool) path: nemerle.lang/language@_section
#. (itstool) path: netrexx.lang/language@_section
#. (itstool) path: objc.lang/language@_section
#. (itstool) path: objj.lang/language@_section
#. (itstool) path: ocaml.lang/language@_section
#. (itstool) path: ooc.lang/language@_section
#. (itstool) path: opal.lang/language@_section
#. (itstool) path: opencl.lang/language@_section
#. (itstool) path: pascal.lang/language@_section
#. (itstool) path: pig.lang/language@_section
#. (itstool) path: prolog.lang/language@_section
#. (itstool) path: rust.lang/language@_section
#. (itstool) path: scala.lang/language@_section
#. (itstool) path: scheme.lang/language@_section
#. (itstool) path: sml.lang/language@_section
#. (itstool) path: sparql.lang/language@_section
#. (itstool) path: sql.lang/language@_section
#. (itstool) path: swift.lang/language@_section
#. (itstool) path: systemverilog.lang/language@_section
#. (itstool) path: thrift.lang/language@_section
#. (itstool) path: vala.lang/language@_section
#. (itstool) path: vbnet.lang/language@_section
#. (itstool) path: verilog.lang/language@_section
#. (itstool) path: vhdl.lang/language@_section
#: abnf.lang:30 actionscript.lang:24 ada.lang:25 ansforth94.lang:24 asp.lang:23
#: automake.lang:23 bennugd.lang:22 bluespec.lang:21 boo.lang:23 cg.lang:23
#: chdr.lang:24 c.lang:24 cobol.lang:26 cpphdr.lang:24 cpp.lang:24
#: csharp.lang:26 cuda.lang:22 d.lang:29 eiffel.lang:23 erlang.lang:23
#: forth.lang:23 fortran.lang:24 fsharp.lang:24 genie.lang:23 glsl.lang:30
#: go.lang:24 groovy.lang:24 haskell.lang:24 haskell-literate.lang:23
#: haxe.lang:34 idl.lang:23 java.lang:24 j.lang:23 kotlin.lang:24 lex.lang:24
#: llvm.lang:22 logtalk.lang:23 makefile.lang:22 meson.lang:23 nemerle.lang:23
#: netrexx.lang:23 objc.lang:23 objj.lang:25 ocaml.lang:26 ooc.lang:23
#: opal.lang:23 opencl.lang:23 pascal.lang:24 pig.lang:26 prolog.lang:23
#: rust.lang:35 scala.lang:24 scheme.lang:23 sml.lang:23 sparql.lang:23
#: sql.lang:23 swift.lang:24 systemverilog.lang:21 thrift.lang:20 vala.lang:27
#: vbnet.lang:23 verilog.lang:23 vhdl.lang:23
#, fuzzy
msgid "Source"
msgstr "منابع"

#. (itstool) path: awk.lang/language@_section
#. (itstool) path: dosbatch.lang/language@_section
#. (itstool) path: javascript.lang/language@_section
#. (itstool) path: lua.lang/language@_section
#. (itstool) path: m4.lang/language@_section
#. (itstool) path: perl.lang/language@_section
#. (itstool) path: php.lang/language@_section
#. (itstool) path: python3.lang/language@_section
#. (itstool) path: python.lang/language@_section
#. (itstool) path: ruby.lang/language@_section
#. (itstool) path: sh.lang/language@_section
#. (itstool) path: tcl.lang/language@_section
#: awk.lang:23 dosbatch.lang:23 javascript.lang:26 lua.lang:23 m4.lang:23
#: perl.lang:25 php.lang:28 python3.lang:23 python.lang:27 ruby.lang:26
#: sh.lang:24 tcl.lang:23
#, fuzzy
msgid "Script"
msgstr "کد‌نوشته‌ها"

#. (itstool) path: bibtex.lang/language@_section
#. (itstool) path: docbook.lang/language@_section
#. (itstool) path: dtd.lang/language@_section
#. (itstool) path: dtl.lang/language@_section
#. (itstool) path: gtk-doc.lang/language@_section
#. (itstool) path: haddock.lang/language@_section
#. (itstool) path: html.lang/language@_section
#. (itstool) path: jade.lang/language@_section
#. (itstool) path: latex.lang/language@_section
#. (itstool) path: mallard.lang/language@_section
#. (itstool) path: markdown.lang/language@_section
#. (itstool) path: mediawiki.lang/language@_section
#. (itstool) path: mxml.lang/language@_section
#. (itstool) path: rst.lang/language@_section
#. (itstool) path: sweave.lang/language@_section
#. (itstool) path: t2t.lang/language@_section
#. (itstool) path: tera.lang/language@_section
#. (itstool) path: texinfo.lang/language@_section
#. (itstool) path: xml.lang/language@_section
#. (itstool) path: xslt.lang/language@_section
#: bibtex.lang:23 docbook.lang:23 dtd.lang:23 dtl.lang:25 gtk-doc.lang:24
#: haddock.lang:23 html.lang:24 jade.lang:24 latex.lang:24 mallard.lang:22
#: markdown.lang:25 mediawiki.lang:22 mxml.lang:23 rst.lang:22 sweave.lang:24
#: t2t.lang:23 tera.lang:23 texinfo.lang:24 xml.lang:25 xslt.lang:23
msgid "Markup"
msgstr "نشان‌گذاری"

#. (itstool) path: cg.lang/language@_name
#: cg.lang:23
#, fuzzy
msgid "CG Shader Language"
msgstr "زبان"

#. (itstool) path: changelog.lang/language@_section
#. (itstool) path: cmake.lang/language@_section
#. (itstool) path: css.lang/language@_section
#. (itstool) path: csv.lang/language@_section
#. (itstool) path: desktop.lang/language@_section
#. (itstool) path: diff.lang/language@_section
#. (itstool) path: dot.lang/language@_section
#. (itstool) path: dpatch.lang/language@_section
#. (itstool) path: gdb-log.lang/language@_section
#. (itstool) path: gtkrc.lang/language@_section
#. (itstool) path: ini.lang/language@_section
#. (itstool) path: json.lang/language@_section
#. (itstool) path: less.lang/language@_section
#. (itstool) path: libtool.lang/language@_section
#. (itstool) path: logcat.lang/language@_section
#. (itstool) path: nsis.lang/language@_section
#. (itstool) path: ocl.lang/language@_section
#. (itstool) path: pkgconfig.lang/language@_section
#. (itstool) path: po.lang/language@_section
#. (itstool) path: protobuf.lang/language@_section
#. (itstool) path: puppet.lang/language@_section
#. (itstool) path: rpmspec.lang/language@_section
#. (itstool) path: scss.lang/language@_section
#. (itstool) path: toml.lang/language@_section
#. (itstool) path: yacc.lang/language@_section
#. (itstool) path: yaml.lang/language@_section
#: changelog.lang:24 cmake.lang:23 css.lang:26 csv.lang:23 desktop.lang:24
#: diff.lang:23 dot.lang:23 dpatch.lang:23 gdb-log.lang:18 gtkrc.lang:24
#: ini.lang:22 json.lang:29 less.lang:23 libtool.lang:23 logcat.lang:23
#: nsis.lang:23 ocl.lang:32 pkgconfig.lang:23 po.lang:23 protobuf.lang:21
#: puppet.lang:23 rpmspec.lang:24 scss.lang:23 toml.lang:23 yacc.lang:23
#: yaml.lang:21
#, fuzzy
msgid "Other"
msgstr "دیگران"

#. (itstool) path: chdr.lang/language@_name
#: chdr.lang:24
msgid "C/ObjC Header"
msgstr ""

#. (itstool) path: cpphdr.lang/language@_name
#: cpphdr.lang:24
#, fuzzy
msgid "C++ Header"
msgstr "چاپ سرصفحه"

#. (itstool) path: dosbatch.lang/language@_name
#: dosbatch.lang:23
msgid "DOS Batch"
msgstr ""

#. (itstool) path: dtl.lang/language@_name
#: dtl.lang:25
msgid "Django Template"
msgstr ""

#. (itstool) path: fcl.lang/language@_section
#. (itstool) path: gap.lang/language@_section
#. (itstool) path: idl-exelis.lang/language@_section
#. (itstool) path: imagej.lang/language@_section
#. (itstool) path: julia.lang/language@_section
#. (itstool) path: matlab.lang/language@_section
#. (itstool) path: maxima.lang/language@_section
#. (itstool) path: modelica.lang/language@_section
#. (itstool) path: octave.lang/language@_section
#. (itstool) path: R.lang/language@_section
#. (itstool) path: scilab.lang/language@_section
#: fcl.lang:23 gap.lang:23 idl-exelis.lang:20 imagej.lang:23 julia.lang:23
#: matlab.lang:25 maxima.lang:24 modelica.lang:25 octave.lang:25 R.lang:25
#: scilab.lang:23
msgid "Scientific"
msgstr ""

#. (itstool) path: gdb-log.lang/language@_name
#: gdb-log.lang:18
msgid "GDB Log"
msgstr ""

#. (itstool) path: glsl.lang/language@_name
#: glsl.lang:30
msgid "OpenGL Shading Language"
msgstr ""

#. (itstool) path: llvm.lang/language@_name
#: llvm.lang:22
msgid "LLVM IR"
msgstr ""

#. (itstool) path: po.lang/language@_name
#: po.lang:23
msgid "gettext translation"
msgstr "ترجمه‌ی gettext"

#. (itstool) path: rpmspec.lang/language@_name
#: rpmspec.lang:24
msgid "RPM spec"
msgstr ""

#. (itstool) path: tera.lang/language@_name
#: tera.lang:23
msgid "Tera Template"
msgstr ""

#. (itstool) path: classic.xml/style-scheme@_name
#: classic.xml:24
msgid "Classic"
msgstr ""

#. (itstool) path: style-scheme/_description
#: classic.xml:26
msgid "Classic color scheme"
msgstr ""

#. (itstool) path: cobalt.xml/style-scheme@_name
#: cobalt.xml:26
msgid "Cobalt"
msgstr ""

#. (itstool) path: style-scheme/_description
#: cobalt.xml:28
msgid "Blue based color scheme"
msgstr ""

#. (itstool) path: kate.xml/style-scheme@_name
#: kate.xml:24
msgid "Kate"
msgstr ""

#. (itstool) path: style-scheme/_description
#: kate.xml:26
msgid "Color scheme used in the Kate text editor"
msgstr ""

#. (itstool) path: oblivion.xml/style-scheme@_name
#: oblivion.xml:25
msgid "Oblivion"
msgstr ""

#. (itstool) path: style-scheme/_description
#: oblivion.xml:28
msgid "Dark color scheme using the Tango color palette"
msgstr ""

#. (itstool) path: solarized-dark.xml/style-scheme@_name
#: solarized-dark.xml:24
msgid "Solarized Dark"
msgstr ""

#. (itstool) path: style-scheme/_description
#: solarized-dark.xml:26
msgid "Color scheme using Solarized dark color palette"
msgstr ""

#. (itstool) path: solarized-light.xml/style-scheme@_name
#: solarized-light.xml:24
msgid "Solarized Light"
msgstr ""

#. (itstool) path: style-scheme/_description
#: solarized-light.xml:26
msgid "Color scheme using Solarized light color palette"
msgstr ""

#. (itstool) path: tango.xml/style-scheme@_name
#: tango.xml:24
msgid "Tango"
msgstr ""

#. (itstool) path: style-scheme/_description
#: tango.xml:26
msgid "Color scheme using Tango color palette"
msgstr ""

#: gtksourceview/completion-providers/words/gtksourcecompletionwords.c:334
msgid "Document Words"
msgstr ""

#: gtksourceview/gtksourcebufferoutputstream.c:843
msgid "Invalid byte sequence in conversion input"
msgstr ""

#: gtksourceview/gtksourcebufferoutputstream.c:849
#, c-format
msgid "Error during conversion: %s"
msgstr ""

#: gtksourceview/gtksourcebufferoutputstream.c:930
#, c-format
msgid "Conversion from character set “%s” to “UTF-8” is not supported"
msgstr ""

#: gtksourceview/gtksourcebufferoutputstream.c:936
#, c-format
msgid "Could not open converter from “%s” to “UTF-8”"
msgstr ""

#: gtksourceview/gtksourcebufferoutputstream.c:996
msgid "Invalid object, not initialized"
msgstr ""

#: gtksourceview/gtksourcebufferoutputstream.c:1188
msgid "Incomplete UTF-8 sequence in input"
msgstr ""

#. Tooltip style
#: gtksourceview/gtksourcecompletioninfo.c:271
msgid "Completion Info"
msgstr ""

#: gtksourceview/gtksourcecompletionmodel.c:495
msgid "Provider"
msgstr ""

#: gtksourceview/gtksourcecompletion.ui:87
msgid "Show detailed proposal information"
msgstr ""

#: gtksourceview/gtksourcecompletion.ui:89
msgid "_Details…"
msgstr ""

#. regex_new could fail, for instance if there are different
#. * named sub-patterns with the same name or if resulting regex is
#. * too long. In this case fixing lang file helps (e.g. renaming
#. * subpatterns, making huge keywords use bigger prefixes, etc.)
#: gtksourceview/gtksourcecontextengine.c:3141
#, c-format
msgid ""
"Cannot create a regex for all the transitions, the syntax highlighting "
"process will be slower than usual.\n"
"The error was: %s"
msgstr ""

#: gtksourceview/gtksourcecontextengine.c:4464
msgid ""
"Highlighting a single line took too much time, syntax highlighting will be "
"disabled"
msgstr ""

#: gtksourceview/gtksourcecontextengine.c:5714
#, c-format
msgid "context “%s” cannot contain a \\%%{...@start} command"
msgstr ""

#: gtksourceview/gtksourcecontextengine.c:5877
#: gtksourceview/gtksourcecontextengine.c:5967
#, c-format
msgid "duplicated context id “%s”"
msgstr ""

#: gtksourceview/gtksourcecontextengine.c:6081
#: gtksourceview/gtksourcecontextengine.c:6141
#, c-format
msgid ""
"style override used with wildcard context reference in language “%s” in ref "
"“%s”"
msgstr ""

#: gtksourceview/gtksourcecontextengine.c:6155
#, c-format
msgid "invalid context reference “%s”"
msgstr ""

#: gtksourceview/gtksourcecontextengine.c:6174
#: gtksourceview/gtksourcecontextengine.c:6184
#, c-format
msgid "unknown context “%s”"
msgstr ""

#: gtksourceview/gtksourcecontextengine.c:6283
#, c-format
msgid "Missing main language definition (id = \"%s\".)"
msgstr ""

#: gtksourceview/gtksourceencoding.c:141 gtksourceview/gtksourceencoding.c:184
#: gtksourceview/gtksourceencoding.c:186 gtksourceview/gtksourceencoding.c:188
#: gtksourceview/gtksourceencoding.c:190 gtksourceview/gtksourceencoding.c:192
#: gtksourceview/gtksourceencoding.c:194 gtksourceview/gtksourceencoding.c:196
#, fuzzy
msgid "Unicode"
msgstr "محدوده‌ی یونی‌کد"

#: gtksourceview/gtksourceencoding.c:155 gtksourceview/gtksourceencoding.c:179
#: gtksourceview/gtksourceencoding.c:229 gtksourceview/gtksourceencoding.c:272
msgid "Western"
msgstr ""

#: gtksourceview/gtksourceencoding.c:157 gtksourceview/gtksourceencoding.c:231
#: gtksourceview/gtksourceencoding.c:268
msgid "Central European"
msgstr ""

#: gtksourceview/gtksourceencoding.c:159
msgid "South European"
msgstr ""

#: gtksourceview/gtksourceencoding.c:161 gtksourceview/gtksourceencoding.c:175
#: gtksourceview/gtksourceencoding.c:282
msgid "Baltic"
msgstr ""

#: gtksourceview/gtksourceencoding.c:163 gtksourceview/gtksourceencoding.c:233
#: gtksourceview/gtksourceencoding.c:246 gtksourceview/gtksourceencoding.c:250
#: gtksourceview/gtksourceencoding.c:252 gtksourceview/gtksourceencoding.c:270
msgid "Cyrillic"
msgstr ""

#: gtksourceview/gtksourceencoding.c:165 gtksourceview/gtksourceencoding.c:239
#: gtksourceview/gtksourceencoding.c:280
msgid "Arabic"
msgstr ""

#: gtksourceview/gtksourceencoding.c:167 gtksourceview/gtksourceencoding.c:274
msgid "Greek"
msgstr ""

#: gtksourceview/gtksourceencoding.c:169
msgid "Hebrew Visual"
msgstr ""

#: gtksourceview/gtksourceencoding.c:171 gtksourceview/gtksourceencoding.c:235
#: gtksourceview/gtksourceencoding.c:276
msgid "Turkish"
msgstr ""

#: gtksourceview/gtksourceencoding.c:173
msgid "Nordic"
msgstr ""

#: gtksourceview/gtksourceencoding.c:177
msgid "Celtic"
msgstr ""

#: gtksourceview/gtksourceencoding.c:181
msgid "Romanian"
msgstr ""

#: gtksourceview/gtksourceencoding.c:199
msgid "Armenian"
msgstr ""

#: gtksourceview/gtksourceencoding.c:201 gtksourceview/gtksourceencoding.c:203
#: gtksourceview/gtksourceencoding.c:217
msgid "Chinese Traditional"
msgstr ""

#: gtksourceview/gtksourceencoding.c:205
msgid "Cyrillic/Russian"
msgstr ""

#: gtksourceview/gtksourceencoding.c:208 gtksourceview/gtksourceencoding.c:210
#: gtksourceview/gtksourceencoding.c:212 gtksourceview/gtksourceencoding.c:242
#: gtksourceview/gtksourceencoding.c:257
msgid "Japanese"
msgstr ""

#: gtksourceview/gtksourceencoding.c:215 gtksourceview/gtksourceencoding.c:244
#: gtksourceview/gtksourceencoding.c:248 gtksourceview/gtksourceencoding.c:263
msgid "Korean"
msgstr ""

#: gtksourceview/gtksourceencoding.c:220 gtksourceview/gtksourceencoding.c:222
#: gtksourceview/gtksourceencoding.c:224
msgid "Chinese Simplified"
msgstr ""

#: gtksourceview/gtksourceencoding.c:226
msgid "Georgian"
msgstr ""

#: gtksourceview/gtksourceencoding.c:237 gtksourceview/gtksourceencoding.c:278
msgid "Hebrew"
msgstr ""

#: gtksourceview/gtksourceencoding.c:254
msgid "Cyrillic/Ukrainian"
msgstr ""

#: gtksourceview/gtksourceencoding.c:259 gtksourceview/gtksourceencoding.c:265
#: gtksourceview/gtksourceencoding.c:284
msgid "Vietnamese"
msgstr ""

#: gtksourceview/gtksourceencoding.c:261
msgid "Thai"
msgstr ""

#: gtksourceview/gtksourceencoding.c:495
msgid "Unknown"
msgstr ""

#. Translators: This is the sorted list of encodings used by
#. * GtkSourceView for automatic detection of the file encoding. You may
#. * want to customize it adding encodings that are common in your
#. * country, for instance the GB18030 encoding for the Chinese
#. * translation. You may also want to remove the ISO-8859-15 encoding
#. * (covering English and most Western European languages) if you think
#. * people in your country will rarely use it.  "CURRENT" is a magic
#. * value used by GtkSourceView and it represents the encoding for the
#. * current locale, so please don't translate the "CURRENT" term.  Only
#. * recognized encodings are used. See
#. * https://git.gnome.org/browse/gtksourceview/tree/gtksourceview/gtksourceencoding.c#n147
#. * for a list of supported encodings.
#. * Keep the same format: square brackets, single quotes, commas.
#.
#: gtksourceview/gtksourceencoding.c:639
msgid "['UTF-8', 'CURRENT', 'ISO-8859-15', 'UTF-16']"
msgstr "['UTF-8', 'CURRENT', 'ISO-8859-15', 'UTF-16']"

#: gtksourceview/gtksourcefileloader.c:516
msgid ""
"There was a character encoding conversion error and it was needed to use a "
"fallback character."
msgstr ""

#: gtksourceview/gtksourcefileloader.c:623
msgid "File too big."
msgstr ""

#: gtksourceview/gtksourcefileloader.c:777
msgid "Not a regular file."
msgstr ""

#: gtksourceview/gtksourcefilesaver.c:976
msgid "The file is externally modified."
msgstr ""

#: gtksourceview/gtksourcefilesaver.c:1428
msgid "The buffer contains invalid characters."
msgstr ""

#. *
#. * SECTION:language
#. * @Short_description: Represents a syntax highlighted language
#. * @Title: GtkSourceLanguage
#. * @See_also: #GtkSourceLanguageManager
#. *
#. * A #GtkSourceLanguage represents a programming or markup language, affecting
#. * syntax highlighting and [context classes][context-classes].
#. *
#. * Use #GtkSourceLanguageManager to obtain a #GtkSourceLanguage instance, and
#. * gtk_source_buffer_set_language() to apply it to a #GtkSourceBuffer.
#.
#: gtksourceview/gtksourcelanguage.c:56
msgid "Others"
msgstr "دیگران"

#: gtksourceview/gtksourcelanguage-parser-2.c:950
#, c-format
msgid "Unknown id “%s” in regex “%s”"
msgstr ""

#: gtksourceview/gtksourcelanguage-parser-2.c:1180
#, c-format
msgid "in regex “%s”: backreferences are not supported"
msgstr ""

#: gtksourceview/gtksourceregex.c:129
msgid "using \\C is not supported in language definitions"
msgstr ""

#. TODO: have a ChooserDialog?
#: gtksourceview/gtksourcestyleschemechooserbutton.c:185
msgid "Select a Style"
msgstr ""

#: gtksourceview/gtksourcestyleschemechooserbutton.c:189
msgid "_Cancel"
msgstr ""

#: gtksourceview/gtksourcestyleschemechooserbutton.c:190
msgid "_Select"
msgstr ""

#. create redo menu_item.
#: gtksourceview/gtksourceview.c:1781
msgid "_Redo"
msgstr ""

#. create undo menu_item.
#: gtksourceview/gtksourceview.c:1792
msgid "_Undo"
msgstr ""

#: gtksourceview/gtksourceview.c:1811
msgid "All _Upper Case"
msgstr ""

#: gtksourceview/gtksourceview.c:1821
msgid "All _Lower Case"
msgstr ""

#: gtksourceview/gtksourceview.c:1831
msgid "_Invert Case"
msgstr ""

#: gtksourceview/gtksourceview.c:1841
msgid "_Title Case"
msgstr ""

#: gtksourceview/gtksourceview.c:1851
#, fuzzy
msgid "C_hange Case"
msgstr "سطر تغییرکرده"

#~ msgid "Escape Character"
#~ msgstr "نویسه‌ی گریز"

#~ msgid "Escaping character for syntax patterns"
#~ msgstr "نوسه‌ی گریز برای الگوهای نحوی"

#~ msgid "Check Brackets"
#~ msgstr "بررسی قلاب‌ها"

#~ msgid "Whether to check and highlight matching brackets"
#~ msgstr "این که آیا قلاب‌ها بررسی شود و قلاب‌های متناظر پررنگ شود"

#~ msgid "Highlight"
#~ msgstr "پررنگ‌سازی"

#~ msgid "Whether to highlight syntax in the buffer"
#~ msgstr "این که آیا پررنگ‌سازی نحوی در میاگیر انجام شود یا نه"

#~ msgid "Maximum Undo Levels"
#~ msgstr "حداکثر سطوح برکرداندن"

#~ msgid "Number of undo levels for the buffer"
#~ msgstr "تعداد سطح‌های برکردان برای میانگیر"

#~ msgid "Language object to get highlighting patterns from"
#~ msgstr "شیء زبانی که الگوهای پررنگ‌سازی از آنجا گرفته می‌شود"

#~ msgid "Language specification directories"
#~ msgstr "شاخه‌های مشخصه‌ی زبان"

#~ msgid ""
#~ "List of directories where the language specification files (.lang) are "
#~ "located"
#~ msgstr "فهرست شاخه‌هایی که پرونده‌های مشخصه‌ی زبان (‎.lang) در آنها قرار دارند."

#~ msgid "Configuration"
#~ msgstr "پیکربندی"

#~ msgid "Configuration options for the print job"
#~ msgstr "گزینه‌های پیکربندی برای امر چاپ"

#~ msgid "Source Buffer"
#~ msgstr "میانگیر مبدأ"

#~ msgid "GtkSourceBuffer object to print"
#~ msgstr "شیء GtkSourceBuffer برای چاپ"

#~ msgid "Tabs Width"
#~ msgstr "عرض جدول‌بندی"

#~ msgid "Width in equivalent space characters of tabs"
#~ msgstr "تعداد نویسه‌های فاصله با عرضی برابر جدول‌بندی"

#~ msgid "Wrap Mode"
#~ msgstr "حالت پیچش"

#~ msgid "Word wrapping mode"
#~ msgstr "حالت پیچش کلمه"

#~ msgid "Whether to print the document with highlighted syntax"
#~ msgstr "این که آیا نوشتار با پررنگ‌سازی نحوی چاپ شود یا نه"

#~ msgid "Font"
#~ msgstr "قلم"

#~ msgid "GnomeFont name to use for the document text (deprecated)"
#~ msgstr "نام قلم گنومی که برای متن نوشتار استفاده می‌شود (منسوخ شده)"

#~ msgid "Font Description"
#~ msgstr "شرح قلم"

#~ msgid "Font to use for the document text (e.g. \"Monospace 10\")"
#~ msgstr "قلمی که برای متن نوشتار استفاده می‌شود (مثلاً «Monospace 10»)"

#~ msgid "Numbers Font"
#~ msgstr "قلم شماره‌ها"

#~ msgid "GnomeFont name to use for the line numbers (deprecated)"
#~ msgstr "نام قلم گنومی که برای شماره‌ی سطرها استفاده می‌شود(منسوخ شده)"

#~ msgid "Font description to use for the line numbers"
#~ msgstr "شرح قلمی که برای شماره‌ی سطرها استفاده می‌شود"

#~ msgid "Print Line Numbers"
#~ msgstr "چاپ شماره‌ی سطرها"

#~ msgid "Interval of printed line numbers (0 means no numbers)"
#~ msgstr "بازه‌ی شماره سطرهای چاپ شده (۰ یعنی بدون شماره)"

#~ msgid "Whether to print a header in each page"
#~ msgstr "این که آیا سرصفحه در تک تک صفحات چاپ شود یا نه"

#~ msgid "Print Footer"
#~ msgstr "چاپ پاصفحه"

#~ msgid "Whether to print a footer in each page"
#~ msgstr "این که آیا پاصفحه در تک تک صفحات چاپ شود یا نه"

#~ msgid "Header and Footer Font"
#~ msgstr "قلم سرصفحه و پاصفحه"

#~ msgid "GnomeFont name to use for the header and footer (deprecated)"
#~ msgstr "نام قلم کنومی که برای سرصفحه و پاصفحه استفاده می‌شود(منسوخ شده)"

#~ msgid "Header and Footer Font Description"
#~ msgstr "شرح قلم سرصفحه و پا صفحه"

#~ msgid "Font to use for headers and footers (e.g. \"Monospace 10\")"
#~ msgstr "قلمی که برای سرصفحه‌ها و پاصفحه‌ها استفاده می‌شود(مثلاً «Monospace 10»)"

#~ msgid "Base-N Integer"
#~ msgstr "عدد مبنای N"

#~ msgid "Character"
#~ msgstr "نویسه"

#~ msgid "Comment"
#~ msgstr "توضیح"

#~ msgid "Data Type"
#~ msgstr "نوع داده"

#~ msgid "Function"
#~ msgstr "تابع"

#~ msgid "Decimal"
#~ msgstr "دهدهی"

#~ msgid "Floating Point"
#~ msgstr "ممیز شناور"

#~ msgid "Keyword"
#~ msgstr "کلیدواژه"

#~ msgid "Preprocessor"
#~ msgstr "پیش‌پردازنده"

#~ msgid "String"
#~ msgstr "رشته"

#~ msgid "Specials"
#~ msgstr "ویژه‌ها"

#~ msgid "Others 2"
#~ msgstr "دیگران ۲"

#~ msgid "Others 3"
#~ msgstr "دیگران ۳"

#~ msgid "Default"
#~ msgstr "پیش‌فرض"

#~ msgid "Tag ID"
#~ msgstr "شناسه‌ی برچسب"

#~ msgid "ID used to refer to the source tag"
#~ msgstr "شناسه‌‌ای که برای ارجاع به برچسب مبدأ استفاده می‌شود"

#~ msgid "Tag style"
#~ msgstr "سبک برچسب"

#~ msgid "The style associated with the source tag"
#~ msgstr "سبک وابسته به برچسب مبدأ"

#~ msgid "Show Line Numbers"
#~ msgstr "نشان دادن شماره‌ی سطرها"

#~ msgid "Whether to display line numbers"
#~ msgstr "این که آیا شماره‌ی خطوط نمایش داده شود یا نه"

#~ msgid "Show Line Markers"
#~ msgstr "نشان دادن نشانگرهای خط"

#~ msgid "Auto Indentation"
#~ msgstr "دندانه‌دار کردن خودکار"

#~ msgid "Whether to enable auto indentation"
#~ msgstr "این که آیا دندانه‌دار کردن خودکار به کار انداخته شود"

#~ msgid "Insert Spaces Instead of Tabs"
#~ msgstr "درچ فاصله به جای جدول‌بندی"

#~ msgid "Whether to insert spaces instead of tabs"
#~ msgstr "این که آیا جای نویسه‌های جدول‌بندی فاصله‌ قرار بگیرد یا نه"

#~ msgid "Show Right Margin"
#~ msgstr "نشان دادن حاشیه‌ی راست"

#~ msgid "Whether to display the right margin"
#~ msgstr "این که آیا حاشیه‌ی راست نمایش داده شود یا نه"

#~ msgid "Margin position"
#~ msgstr "موقعیت حاشیه"

#~ msgid "Position of the right margin"
#~ msgstr "موقعیت حاشیه‌ی راست"

#~ msgid "Use smart home/end"
#~ msgstr "استفاده از کلیدهای آغازه و پایان‌بر هوشمند"

#~ msgid ""
#~ "HOME and END keys move to first/last non whitespace characters on line "
#~ "before going to the start/end of the line"
#~ msgstr ""
#~ "کلید‌های آغازه و پایان‌بر اول به اولین/آخرین نویسه‌های غیر فاصله‌ای سطر "
#~ "می‌روند و سپس به اول/آخر سطر  "

#~ msgid "Highlight current line"
#~ msgstr "پررنگ کردن خط فعلی"

#~ msgid "Whether to highlight the current line"
#~ msgstr "این که آیا خط فعلی پررنگ شود یا نه"

#~ msgid "Ada"
#~ msgstr "آدا"

#~ msgid "Character Constant"
#~ msgstr "ثابت نویسه‌ای"

#~ msgid "Keywords"
#~ msgstr "کلیدواژه‌ها"

#~ msgid "Line Comment"
#~ msgstr "توضیح سطری"

#~ msgid "Number"
#~ msgstr "شماره"

#~ msgid "Preprocessor Keywords"
#~ msgstr "کلیدواژه‌های پیش‌پردازنده"

#~ msgid "True And False"
#~ msgstr "درست و نادرست"

#~ msgid "Types"
#~ msgstr "نوع‌ها"

#~ msgid "'#if 0' Comment"
#~ msgstr "توضیح «#if 0»"

#~ msgid "Block Comment"
#~ msgstr "توضیح بلوکی"

#~ msgid "C"
#~ msgstr "C"

#~ msgid "Common Macro"
#~ msgstr "ماکروهای مشترک"

#~ msgid "Floating Point Number"
#~ msgstr "عدد ممیز شناور"

#~ msgid "Hex Number"
#~ msgstr "عدد شانزده‌شانزدهی"

#~ msgid "Include/Pragma"
#~ msgstr "Include/Pragma"

#~ msgid "Octal Number"
#~ msgstr "عدد هشت‌هشتی"

#~ msgid "Preprocessor Definitions"
#~ msgstr "تعریف‌های پیش‌پردازنده"

#~ msgid "C++"
#~ msgstr "C++‎"

#~ msgid "C#"
#~ msgstr "C#‎"

#~ msgid "Multiline String"
#~ msgstr "رشته‌ی چندخطی"

#~ msgid "Primitives"
#~ msgstr "اولیه‌ها"

#~ msgid "String 2"
#~ msgstr "رشته‌ی ۲"

#~ msgid "Attribute Value Delimiters"
#~ msgstr "جداساز مقادیر مشخصه"

#~ msgid "Dimension"
#~ msgstr "بُعد"

#~ msgid "Hexadecimal Color"
#~ msgstr "رنگ شانزده‌شانزدهی"

#~ msgid "Importance Modifier"
#~ msgstr "تغییر دهنده‌ی اهمیّت"

#~ msgid "Known Property Values"
#~ msgstr "مقدار ویژگی‌های معلوم"

#~ msgid "Operators"
#~ msgstr "عملگرها"

#~ msgid "Property Names"
#~ msgstr "نام‌ ویژگی‌ها"

#~ msgid "Selector Grammar"
#~ msgstr "دستور زبان انتخاب‌گر"

#~ msgid "Selector Pseudo Classes"
#~ msgstr "شبه‌رده‌های انتخاب‌گر"

#~ msgid "Selector Pseudo Elements"
#~ msgstr "شبه‌عنصرهای انتخاب‌گر"

#~ msgid "Unicode Character Reference"
#~ msgstr "ارجاع نویسه‌ی یونی‌کد"

#~ msgid ".desktop"
#~ msgstr ".desktop"

#~ msgid "Boolean Value"
#~ msgstr "مقدار بولی"

#~ msgid "Encoding"
#~ msgstr "کدگذاری"

#~ msgid "Exec Parameter"
#~ msgstr "پارامترهای اجرا"

#~ msgid "Group"
#~ msgstr "گروه"

#~ msgid "Non Standard Key"
#~ msgstr "کلید نااستاندارد"

#~ msgid "Standard Key"
#~ msgstr "کلید استاندارد"

#~ msgid "Added line"
#~ msgstr "سطر اضافه‌شده"

#~ msgid "Diff"
#~ msgstr "Diff"

#~ msgid "Diff file"
#~ msgstr "پرونده‌ی Diff"

#~ msgid "Location"
#~ msgstr "مکان"

#~ msgid "Removed line"
#~ msgstr "سطر حذف‌شده"

#~ msgid "Special case"
#~ msgstr "مورد ویژه"

#~ msgid "Attributes"
#~ msgstr "مشخصه‌ها"

#~ msgid "Closing Bracket"
#~ msgstr "بستن قلاب"

#~ msgid "DTD"
#~ msgstr "DTD"

#~ msgid "Entity"
#~ msgstr "نهاد"

#~ msgid "HTML"
#~ msgstr "HTML"

#~ msgid "Tags"
#~ msgstr "برچسب‌ها"

#~ msgid "Fortran 95"
#~ msgstr "فرترن ۹۵"

#~ msgid "Input/Output"
#~ msgstr "ورودی/خروجی"

#~ msgid "Read/Write"
#~ msgstr "خواندن/نوشتن"

#~ msgid "Double Quote String"
#~ msgstr "رشته‌ی داخل علامت نقل‌قول دوتایی"

#~ msgid "GtkRC"
#~ msgstr "GtkRC"

#~ msgid "Include Directive"
#~ msgstr "شامل رهنمود"

#~ msgid "Single Quote String"
#~ msgstr "رشته‌ی داخل علامت نقل قول تکی"

#~ msgid "State"
#~ msgstr "وضعیت"

#~ msgid "Haskell"
#~ msgstr "Haskell"

#~ msgid "Type or Constructor"
#~ msgstr "نوع یا سازنده"

#~ msgid "IDL"
#~ msgstr "IDL"

#~ msgid ".ini"
#~ msgstr ".ini"

#~ msgid "Decimal Number"
#~ msgstr "عدد دهدهی"

#~ msgid "Integer"
#~ msgstr "عدد صحیح"

#~ msgid "Declarations"
#~ msgstr "اعلام‌ها"

#~ msgid "Flow"
#~ msgstr "جریان"

#~ msgid "Java"
#~ msgstr "جاوا"

#~ msgid "Memory"
#~ msgstr "حافظه"

#~ msgid "Modifiers"
#~ msgstr "تغییردهنده‌ها"

#~ msgid "Numeric"
#~ msgstr "عددی"

#~ msgid "Array Operators"
#~ msgstr "عملگرهای آرایه‌ای"

#~ msgid "Constructors"
#~ msgstr "سازنده‌ها"

#~ msgid "Error Handling"
#~ msgstr "رفعِ خطا"

#~ msgid "Floating-Point Number"
#~ msgstr "عدد ممیز شناور"

#~ msgid "Flow Keywords"
#~ msgstr "کلیدواژه‌های جریان"

#~ msgid "Future Reserved Words"
#~ msgstr "کلمه‌های رزروشده‌ی آینده"

#~ msgid "Global Functions"
#~ msgstr "توابع سراسری"

#~ msgid "Global Properties"
#~ msgstr "ویژگی‌های سراسری"

#~ msgid "Hexadecimal Number"
#~ msgstr "عدد شانزده‌شانزدهی"

#~ msgid "Literals"
#~ msgstr "نویسه‌های لفظی"

#~ msgid "Math Value Properties"
#~ msgstr "ویژگی‌های مقدار ریاضی"

#~ msgid "Object Functions"
#~ msgstr "توابع هدف"

#~ msgid "Object Properties"
#~ msgstr "ویژگی‌های شیء"

#~ msgid "Objects"
#~ msgstr "شیءها"

#~ msgid "Unicode Escape Sequence"
#~ msgstr "دنباله‌ی گریز یونی‌کدی"

#~ msgid "Word Operators"
#~ msgstr "عملگرهای کلمه‌ای"

#~ msgid "Comment Environment"
#~ msgstr "محیط توضیح"

#~ msgid "Include"
#~ msgstr "درج"

#~ msgid "LaTeX"
#~ msgstr "LaTeX"

#~ msgid "Math"
#~ msgstr "ریاضی"

#~ msgid "Most Used Commands"
#~ msgstr "فرمان‌هایی که بیشتر استفاده ‌شده"

#~ msgid "Functions"
#~ msgstr "توابع"

#~ msgid "Line Comment 2"
#~ msgstr "توضیح سطری ۲"

#~ msgid "Lua"
#~ msgstr "Lua"

#~ msgid "Multiline String 2"
#~ msgstr "رشته‌ی چندخطی ۲"

#~ msgid "Backtick String"
#~ msgstr "رشته‌ی تیک‌وارویی"

#~ msgid "Directives"
#~ msgstr "رهنمودها"

#~ msgid "Makefile"
#~ msgstr "Makefile"

#~ msgid "Special Targets"
#~ msgstr "هدف‌های ویژه"

#~ msgid "Targets"
#~ msgstr "هدف‌ها"

#~ msgid "Variable1"
#~ msgstr "متغیر ۱"

#~ msgid "Variable2"
#~ msgstr "متغیر ۲"

#~ msgid "Binary Number"
#~ msgstr "عدد دودویی"

#~ msgid "Core Keywords"
#~ msgstr "کلیدواژه‌های اصلی"

#~ msgid "Core Types"
#~ msgstr "نوع‌های اصلی"

#~ msgid "Keywords Defined by Macros"
#~ msgstr "کلیدواژه‌های که به وسیله‌ی ماکرو تعریف شده‌اند"

#~ msgid "Block Comment 1"
#~ msgstr "توضیح بلوکی ۱"

#~ msgid "Block Comment 2"
#~ msgstr "توضیح بلوکی ۲"

#~ msgid "Boolean Bitwise Operators"
#~ msgstr "عملگرهای بیتی بولی"

#~ msgid "Builtin Functions"
#~ msgstr "توابع توکار"

#~ msgid "Builtin Types"
#~ msgstr "نوع‌های داخلی"

#~ msgid "Builtin Values"
#~ msgstr "مقدارهای بولی"

#~ msgid "Functions and Function Modifiers"
#~ msgstr "توابع و تغییردهنده‌های توابع"

#~ msgid "General Format"
#~ msgstr "قالب کلی"

#~ msgid "Loop, Flow, and Exceptions Keywords"
#~ msgstr "کلیدواژه‌های حلقه، جریان، و استثنا"

#~ msgid "Math Operators"
#~ msgstr "عملگرهای ریاضی"

#~ msgid "Pascal"
#~ msgstr "پاسکال"

#~ msgid "Preprocessor Defines"
#~ msgstr "تعریف‌های پیش‌پردازنده"

#~ msgid "Type, Class and Object Keywords"
#~ msgstr "کلیدواژه‌های نوع، رده و شیء"

#~ msgid "Function Call"
#~ msgstr "فراخواندن تابع"

#~ msgid "Method Call"
#~ msgstr "فراخواندن روش"

#~ msgid "Perl"
#~ msgstr "پرل"

#~ msgid "String2"
#~ msgstr "رشته‌ی ۲"

#~ msgid "String3"
#~ msgstr "رشته‌ی ۳"

#~ msgid "Variables"
#~ msgstr "متغیرها"

#~ msgid "Bash Line Comment"
#~ msgstr "توضیح سطری bash"

#~ msgid "C Block Comment"
#~ msgstr "توضیح بلوکی C"

#~ msgid "C++ Line Comment"
#~ msgstr "توضیح سطری C++"

#~ msgid "Case Insensitive Keyword"
#~ msgstr "کلیدواژه‌ی غیرحساس به بزرگی و کوچکی"

#~ msgid "Case Sensitive Keyword"
#~ msgstr "کلیدواژه‌ی حساس به بزرگی و کوچکی"

#~ msgid "Double Quoted String"
#~ msgstr "رشته‌ی داخل علامت نقل‌قول دوتایی"

#~ msgid "HTML Block"
#~ msgstr "بلوک HTML"

#~ msgid "PHP"
#~ msgstr "PHP"

#~ msgid "Single Quoted String"
#~ msgstr "رشته‌ی داخل علامت نقل‌قول تکی"

#~ msgid "Format"
#~ msgstr "قالب"

#~ msgid "Builtins"
#~ msgstr "داخلی‌ها"

#~ msgid "Module Handler"
#~ msgstr "متصدی پیمانه‌"

#~ msgid "Python"
#~ msgstr "پیتون"

#~ msgid "Assignment Operator"
#~ msgstr "عملگر انتساب"

#~ msgid "Delimiter"
#~ msgstr "جداساز"

#~ msgid "Integer Number"
#~ msgstr "عدد صحیح"

#~ msgid "Reserved Class"
#~ msgstr "رده‌ی رزرو شده"

#~ msgid "Reserved Constant"
#~ msgstr "ثابت رزرو شده"

#~ msgid "Attribute Definitions"
#~ msgstr "تعریف‌ مشخصه‌ها"

#~ msgid "Class Variables"
#~ msgstr "متغیرهای رده"

#~ msgid "Constants"
#~ msgstr "ثابت‌ها"

#~ msgid "Definitions"
#~ msgstr "تعریف‌ها"

#~ msgid "Global Variables"
#~ msgstr "متغیرهای سراسری"

#~ msgid "Instance Variables"
#~ msgstr "متغیرهای نمونه"

#~ msgid "Module Handlers"
#~ msgstr "متصدی‌های پیمانه"

#~ msgid "Multiline Comment"
#~ msgstr "توضیح چندخطی"

#~ msgid "Pseudo Variables"
#~ msgstr "شبه‌متغیرها"

#~ msgid "RegExp Variables"
#~ msgstr "متغیرهای RegExp"

#~ msgid "Regular Expression"
#~ msgstr "عبارت منظم"

#~ msgid "Regular Expression 2"
#~ msgstr "عبارت منظم ۲"

#~ msgid "Ruby"
#~ msgstr "Ruby"

#~ msgid "Built-in Commands"
#~ msgstr "فرمان‌های توکار"

#~ msgid "Common Commands"
#~ msgstr "فرمان‌های مشترک"

#~ msgid "Redirections"
#~ msgstr "تغییر مسیرها"

#~ msgid "Self"
#~ msgstr "خود"

#~ msgid "ANSI Datatypes"
#~ msgstr "نوع داده‌های ANSI"

#~ msgid "ANSI Reserved Words"
#~ msgstr "واژه‌های رزرو شده‌ی ANSI"

#~ msgid "Aggregate Functions"
#~ msgstr "توابع انبوه‌ای"

#~ msgid "Analytic Functions"
#~ msgstr "توابع تحلیلی"

#~ msgid "Character Functions Returning Character Values"
#~ msgstr "توابع نویسه‌ای که مقدار نویسه‌ای بازمی‌گردانند"

#~ msgid "Character Functions Returning Number Values"
#~ msgstr "توابع نویسه‌ای که مقدار عددی بازمی‌گردانند"

#~ msgid "Conditions"
#~ msgstr "شرط‌ها"

#~ msgid "Conversion Functions"
#~ msgstr "توابع تبدیلی"

#~ msgid "Data Mining Functions"
#~ msgstr "توابع استخراج داده"

#~ msgid "Datetime Functions"
#~ msgstr "توابع تقویم و زمان"

#~ msgid "Encoding and Decoding Functions"
#~ msgstr "توابع کدگذاری و کدگشایی"

#~ msgid "Environment and Identifier Functions"
#~ msgstr "توابع محیطی و شناسه‌ای"

#~ msgid "General Comparison Functions"
#~ msgstr "توابع مقایسه‌ی همگانی"

#~ msgid "Hierarchical Function"
#~ msgstr "توابع سلسله‌مراتبی"

#~ msgid "Integer Literals"
#~ msgstr "عدد صحیح‌های صریح"

#~ msgid "Large Object Functions"
#~ msgstr "توابع شیئی بزرگ"

#~ msgid "Model Functions"
#~ msgstr "توابع مدل"

#~ msgid "NULL-Related Functions"
#~ msgstr "توابع مربوط به Null"

#~ msgid "Null"
#~ msgstr "Null"

#~ msgid "Number Literals"
#~ msgstr "عددهای صریح"

#~ msgid "Numeric Functions"
#~ msgstr "توابع عددی"

#~ msgid "Object Reference Functions"
#~ msgstr "توابع ارجاع به شیء"

#~ msgid "Oracle Built-in Datatypes"
#~ msgstr "نوع داده‌های توکار Oracle"

#~ msgid "Oracle Reserved Words"
#~ msgstr "کلمه‌های رزرو شده‌ی Oracle"

#~ msgid "Oracle-Supplied Types"
#~ msgstr "نوع‌های تعمین شده توسط Oracle"

#~ msgid "SQL"
#~ msgstr "SQL"

#~ msgid "SQL Statements"
#~ msgstr "دستورات SQL"

#~ msgid "SQL*Plus Commands"
#~ msgstr "فرمان‌های SQL*Plus"

#~ msgid "SQL/DS and DB2 Datatypes"
#~ msgstr "نوع داده‌ی SQL/DS و  DB2 "

#~ msgid "Text Literals"
#~ msgstr "متن‌های صریح"

#~ msgid "Unlimited"
#~ msgstr "نامحدود"

#~ msgid "XML Functions"
#~ msgstr "توابع XML"

#~ msgid "Tcl"
#~ msgstr "Tcl"

#~ msgid "Characters"
#~ msgstr "نویسه‌ها"

#~ msgid "File Attributes"
#~ msgstr "مشخصه‌های پرونده"

#~ msgid "Formatting"
#~ msgstr "قالب‌بندی"

#~ msgid "Generated Content"
#~ msgstr "محتویات تولید شده"

#~ msgid "Indexing"
#~ msgstr "نمایه‌گذاری"

#~ msgid "Macro Parameters"
#~ msgstr "پارامترهای ماکرو"

#~ msgid "Macros"
#~ msgstr "ماکروها"

#~ msgid "Markup (block)"
#~ msgstr "نشان‌گذاری (بلوک)"

#~ msgid "Markup (inline)"
#~ msgstr "نشان‌گذاری (درخط)"

#~ msgid "Texinfo"
#~ msgstr "اطلاعات متن"

#~ msgid "Old Style Comment"
#~ msgstr "توضیح به سبک قدیم"

#~ msgid "VB.NET"
#~ msgstr "VB.NET"

#~ msgid "Gates"
#~ msgstr "گیت‌ها"

#~ msgid "Verilog"
#~ msgstr "Verilog"

#~ msgid "True and False"
#~ msgstr "درست و نادرست"

#~ msgid "Type"
#~ msgstr "نوع‌"

#~ msgid "VHDL"
#~ msgstr "VHDL"

#~ msgid "Hexadecimal"
#~ msgstr "شانزده‌شانزدهی"

#~ msgid "XML"
#~ msgstr "XML"
